# EverQuest Audio Triggers

This is an audio trigger script, for POSIX-compatible systems.

## Requirements

* Python 3.8+ (no extra libs needed)
* espeak-ng (3rd party, install via your package manager)


## Usage


The intended usage is to pipe input lines into the script.
This will allow you to test your audio triggers against old log entries.


* Add everquest-audio-triggers to your path
* Set everquest-audio-triggers to executable
* Set the EQTRIGGERDIR environment variable to the directory containing your triggers
* Pipe log entries into the script


**To watch a log and send notifications**

```bash
export EQTRIGGERDIR="/path/to/triggers/"
tail -f /path/to/everquest/directory/Logs/eqlog_Quikli_mischief.txt | everquest-audio-triggers
```

**To test your audio triggers against an old set of log lines**

```bash
export EQTRIGGERDIR="/path/to/triggers/"
cat /path/to/old/log.txt | everquest-audio-triggers
```

## Examples


You can find example triggers in the triggers directory of this repository.


## Triggers


The triggers should be set up in a directory (EQTRIGGERDIR) as json files containing an array of JSON objects, such as:

```json
[
    {
        "name": "Ferocity Refresh",
        "trigger_text": "^you begin casting ruthless ferocity rk. iii\\.$",
        "voice_alert": "Ferocity",
        "delay": 120
    },
    {
        "name": "Growl",
        "trigger_text": "^you begin casting growl of the panther\\.$",
        "voice_alert": "Growl",
        "delay": 96
    },
    {
        "name": "Tell",
        "trigger_text": "^(?P<player>[a-z]*) tells you, '(?P<message>.*)'$",
        "text_alert": "{player} tells you {message}",
        "timeout": 18000
    }
]
 ```

JSON Object Structure:

* **name** - _required_ - A name for the trigger, will show up as the title in the dbus notification.
* **trigger_text** - _required_ - A python-style case insensitive regular expression to look for.  Can use python-style named match groups like (?P<thing>[a-z]+) to capture the sub-expression into a variable named "thing" to be used in voice or text alerts.
* **text_alert** - _optional_ - A python format string for the body in the dbus notification.  If you captured a variable with trigger_text, you can inject it into this string using curly brances "{thing}".
* **voice_alert** - _optional_ - The same as text_alert, but will output to an espeak-ng subprocess to produce an audible alert with the message.
* **timeout** - _optional_ - The timeout in milliseconds that will be associated with the dbus notification, to automatically close the notification after a certain duration.  Defaults to 10000 (10 seconds).
* **delay** - _optional_ - If set, instead of immediately displaying the notification, waits the set number of seconds (not milliseconds) before displaying/sending the notification.  Useful for things like recast timers.

