#!/usr/bin/env python
from dataclasses import dataclass, field
import json
import os
import re
import subprocess
import sys
import time

from threading import Thread
from typing import Any


# Location of triggers that should be loaded can be multiple files
# the files should be a JSON Array containing objects.

TRIGGER_DIR = os.environ.get("EQTRIGGERDIR")
if not TRIGGER_DIR:
    print("Please set EQTRIGGERDIR environment variable")
    sys.exit(1)


class PriorityQueue:

    def __init__(self):
        self._q = {}

    def put(self, item):
        self._q[item.name] = item

    def get(self):
        for item in self._q.values():
            if item.priority < time.monotonic():
                del self._q[item.name]
                return item


@dataclass(order=True)
class Notification:
    priority: int
    delay: int
    timeout: int
    line: str
    name: str
    voice_alert: str
    text_alert: str
    variables: Any


def send(item):
    title = f"EQ {item.name}"
    voice_message = item.voice_alert
    text_message = item.text_alert
    timeout = item.timeout
    message = text_message or voice_message
    print('NOTIFY', message)
    if voice_message:
        subprocess.run(["espeak-ng", f'"{message}"'], capture_output=True)
    subprocess.run([
        'notify-send',
        '-a', ''.join(title.split(' ')),
        f'--expire-time={timeout}',
        title, message
    ])


def delayed_send_queue(notify_queue):
    while True:
        item = notify_queue.get()
        if item:
            send(item)
        time.sleep(1)


def load_triggers():
    active_triggers = []
    for filename in os.listdir(TRIGGER_DIR):
        with open(os.path.join(TRIGGER_DIR, filename), 'r') as f:
            triggers = json.loads(f.read())
        for trigger in triggers:
            regex = trigger['trigger_text'].strip()
            pattern = re.compile(regex, re.IGNORECASE)
            active_triggers.append((pattern, trigger))
    return active_triggers


def get_lines():
    for line in sys.stdin:
        yield line[27:].strip()


def process_match(line, match, trigger, queue):
    name = trigger.get("name", "")
    timeout = trigger.get("timeout", 10000)
    delay = trigger.get("delay", 0)

    voice_alert_raw = trigger.get('voice_alert', "")
    voice_alert = voice_alert_raw.format(**match)

    text_alert_raw = trigger.get('text_alert', "")
    text_alert = text_alert_raw.format(**match)

    item = Notification(
        priority=time.monotonic() + delay,
        delay=delay,
        timeout=timeout,
        line=line,
        name=name,
        voice_alert=voice_alert,
        text_alert=text_alert,
        variables=match,
    )

    queue.put(item)
    print('MATCH', line, name, match)


def process_matches(matches, queue):
    for line, match, trigger in matches:
        process_match(line, match, trigger, queue)


def match(line, pattern):
    pattern_match = pattern.match(line)
    if not pattern_match:
        return None
    else:
        return pattern_match.groupdict()


def find_matches(lines, triggers):
    for line in lines:
        for pattern, trigger in triggers:
            m = match(line, pattern)
            if m is None:
                continue
            yield line, m, trigger


def main():
    queue = PriorityQueue()
    Thread(target=lambda: delayed_send_queue(queue), daemon=True).start()
    matches = find_matches(get_lines(), load_triggers())
    process_matches(matches, queue)


if __name__ == '__main__':
    main()

